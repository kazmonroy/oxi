import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons/icons.module';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [CommonModule, IconsModule, RouterModule],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  hover = false;
  showMoreClass!: string;

  items = [
    {
      icon: 'home',
      object: 'Home',
      path: '/',
    },

    {
      icon: 'grid',
      object: 'Dashboard',
      path: '/dashboard',
    },
    {
      icon: 'table',
      object: 'Panels',
      path: '/panels',
    },

    {
      icon: 'list',
      object: 'List',
      path: '/list',
    },
    {
      icon: 'star',
      object: 'Favorites',
      path: '/favorites',
    },
  ];

  showMoreIcon() {
    this.showMoreClass = 'show';
  }

  hiddeMoreIcon() {
    this.showMoreClass = '';
  }

  onClick() {
    console.log('clicked!');
  }
}
