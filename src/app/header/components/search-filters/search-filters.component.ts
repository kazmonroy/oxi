import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-search-filters',
  templateUrl: './search-filters.component.html',
  styleUrls: ['./search-filters.component.scss'],
  standalone: true,
  imports: [CommonModule],
})
export class SearchFiltersComponent {
  filters = [
    { name: 'filter 1', entity: 'Customer' },
    { name: 'filter 2', entity: 'Contract' },
    { name: 'filter 3', entity: 'Flex place' },
    { name: 'filter 4', entity: 'Service' },
    { name: 'filter 5', entity: 'Customer' },
    { name: 'filter 6', entity: 'Contract' },
    { name: 'filter 7', entity: 'Flex place' },
    { name: 'filter 8', entity: 'Service' },
    { name: 'filter 9', entity: 'Customer' },
    { name: 'filter 10', entity: 'Contract' },
    { name: 'filter 11', entity: 'Flex place' },
    { name: 'filter 12', entity: 'Service' },
  ];
}
