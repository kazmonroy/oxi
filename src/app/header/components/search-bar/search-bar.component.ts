import { CommonModule } from '@angular/common';
import { Component, HostListener, inject, ElementRef } from '@angular/core';
import { IconsModule } from 'src/app/icons/icons.module';
import { SearchRecentsComponent } from '../search-recents/search-recents.component';
import { SearchFiltersComponent } from '../search-filters/search-filters.component';
import { SearchModule } from 'src/app/search/search.module';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    IconsModule,
    SearchRecentsComponent,
    SearchFiltersComponent,
    ReactiveFormsModule,
  ],
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent {
  private fb = inject(FormBuilder);
  private router = inject(Router);
  private eRef = inject(ElementRef);

  expandRecents = false;
  expandFilters = false;

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.expandRecents = false;
      this.expandFilters = false;
    }
  }

  recents = [
    { key: 'First name', value: 'jonas' },
    { key: 'noFilter', value: '199101311121' },
    { key: 'City', value: 'malmö' },
  ];

  searchForm = this.fb.group({
    query: [''],
  });

  @HostListener('click', ['$event'])
  openRecents(event: KeyboardEvent) {
    event.preventDefault();
    this.expandRecents = true;
  }

  @HostListener('window:keydown.escape', ['$event'])
  closeRecents(event: KeyboardEvent) {
    event.preventDefault();
    this.expandRecents = false;
    this.expandFilters = false;
  }

  onChange(query: any) {
    if (query === '//') {
      this.expandFilters = true;
      this.expandRecents = false;
    } else if (!query) {
      this.expandFilters = false;
      this.expandRecents = true;
    }
  }

  onSubmit() {
    if (this.searchForm.valid) {
      this.router.navigateByUrl('/search');
    }
    console.log(this.searchForm.value);
  }
}
