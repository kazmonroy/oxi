import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { IconsModule } from 'src/app/icons/icons.module';

@Component({
  standalone: true,
  imports: [CommonModule, IconsModule],
  selector: 'app-search-recents',
  templateUrl: './search-recents.component.html',
  styleUrls: ['./search-recents.component.scss'],
})
export class SearchRecentsComponent {
  @Input() recents!: any[];
}
