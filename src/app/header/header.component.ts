import { Component, HostListener } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons/icons.module';
import { SearchRecentsComponent } from './components/search-recents/search-recents.component';
import { AppModule } from '../app.module';
import { SearchBarComponent } from './components/search-bar/search-bar.component';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    CommonModule,
    IconsModule,
    SearchRecentsComponent,
    SearchBarComponent,
  ],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {}
