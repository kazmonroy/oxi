import { Component, inject } from '@angular/core';
import { SearchService } from '../search/services/search.service';
import { Observable } from 'rxjs';
import { Results } from '../search/types/results.interface';
import { SearchDataSource } from '../search/services/search.dataSource';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  private searchService = inject(SearchService);

  results = this.searchService.fetchAll();
  tableView!: boolean;
  boxView!: boolean;

  ngOnInit(): void {
    this.dataSource.loadUsers({ active: 'id', direction: 'asc' });

    this.searchService.fetchAll().subscribe((res) => {
      this.total = res.length;
    });

    this.tableView = true;
  }

  changeView(event: any) {
    const target = event.target!.textContent;
    const cleanTarget = target.trim();
    console.log(cleanTarget);
    if (cleanTarget === 'List') {
      this.tableView = true;
      this.boxView = false;
    } else {
      this.boxView = true;
      this.tableView = false;
    }
  }

  recents: Observable<Results[]> = this.searchService.fetchAll();

  displayedColumns: string[] = ['id', 'name', 'username'];
  currentPage = 0;
  limit: number = 10;
  total = 0;

  dataSource = new SearchDataSource();

  sortResults(sort: Sort) {
    this.dataSource.loadUsers(sort);
  }
}
