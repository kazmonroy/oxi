import { Component, OnInit, inject } from '@angular/core';
import { SearchService } from '../search/services/search.service';
import { Results } from '../search/types/results.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  private searchService = inject(SearchService);

  recents: Observable<Results[]> = this.searchService.fetchAll();

  ngOnInit(): void {}
}
