import { NgModule } from '@angular/core';

import { FeatherModule } from 'angular-feather';
import {
  Bell,
  User,
  Globe,
  Settings,
  Search,
  Mic,
  ChevronRight,
  Home,
  MoreVertical,
  List,
  Grid,
  Star,
  Menu,
  Map,
  Columns,
  ChevronDown,
  Table,
  Clock,
  Folder,
  Plus,
} from 'angular-feather/icons';

const icons = {
  Bell,
  User,
  Globe,
  Settings,
  Search,
  Mic,
  ChevronRight,
  Home,
  MoreVertical,
  List,
  Grid,
  Star,
  Menu,
  Map,
  Columns,
  ChevronDown,
  Table,
  Clock,
  Folder,
  Plus,
};

@NgModule({
  imports: [FeatherModule.pick(icons)],
  exports: [FeatherModule],
})
export class IconsModule {}
