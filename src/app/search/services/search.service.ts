import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Results } from '../types/results.interface';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Sort } from '@angular/material/sort';

@Injectable()
export class SearchService {
  private http = inject(HttpClient);
  fetchSearchResults(sort: Sort): Observable<Results[]> {
    const params = new HttpParams()
      .set('_sort', sort.active)
      .set('_order', sort.direction);
    return this.http.get<Results[]>(
      'https://jsonplaceholder.typicode.com/users',
      { params }
    );
  }

  fetchAll() {
    return this.http.get<Results[]>(
      'https://jsonplaceholder.typicode.com/users'
    );
  }
}
