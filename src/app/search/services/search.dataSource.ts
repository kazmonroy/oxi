import { DataSource } from '@angular/cdk/collections';
import { Injectable, inject } from '@angular/core';
import { Results } from '../types/results.interface';
import { BehaviorSubject, Observable } from 'rxjs';
import { SearchService } from './search.service';
import { Sort } from '@angular/material/sort';

@Injectable()
export class SearchDataSource extends DataSource<Results> {
  private searchService = inject(SearchService);
  results$ = new BehaviorSubject<Results[]>([]);

  constructor() {
    super();
  }

  connect(): Observable<Results[]> {
    return this.results$.asObservable();
  }

  disconnect(): void {
    this.results$.complete();
  }

  loadUsers(sort: Sort) {
    this.searchService.fetchSearchResults(sort).subscribe((results) => {
      this.results$.next(results);
    });
  }
}
