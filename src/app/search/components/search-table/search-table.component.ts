import { Component, OnInit, inject } from '@angular/core';
import { SearchDataSource } from '../../services/search.dataSource';
import { Sort } from '@angular/material/sort';
import { SearchService } from '../../services/search.service';
import { PageEvent } from '@angular/material/paginator';
import { Results } from '../../types/results.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-search-table',
  templateUrl: './search-table.component.html',
  styleUrls: ['./search-table.component.scss'],
})
export class SearchTableComponent implements OnInit {
  private searchService = inject(SearchService);
  displayedColumns: string[] = ['id', 'name', 'username'];
  currentPage = 0;
  limit: number = 10;
  total = 0;

  dataSource = new SearchDataSource();

  ngOnInit(): void {
    this.dataSource.loadUsers({ active: 'id', direction: 'asc' });

    this.searchService.fetchAll().subscribe((res) => {
      this.total = res.length;
    });
  }

  sortResults(sort: Sort) {
    this.dataSource.loadUsers(sort);
  }

  handlePageChange(event: PageEvent) {
    this.currentPage = event.pageIndex;
  }
}
