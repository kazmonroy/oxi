import { Component, inject } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { Observable } from 'rxjs';
import { Results } from '../../types/results.interface';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss'],
})
export class SearchBoxComponent {
  private searchService = inject(SearchService);

  results: Observable<Results[]> = this.searchService.fetchAll();
}
