import { Component, OnInit, inject } from '@angular/core';
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
})
export class SearchResultsComponent implements OnInit {
  private searchService = inject(SearchService);

  results = this.searchService.fetchAll();
  tableView!: boolean;
  boxView!: boolean;
  ngOnInit() {
    this.tableView = true;
  }

  changeView(event: any) {
    const target = event.target!.textContent;
    const cleanTarget = target.trim();
    console.log(cleanTarget);
    if (cleanTarget === 'List') {
      this.tableView = true;
      this.boxView = false;
    } else {
      this.boxView = true;
      this.tableView = false;
    }
  }
}
