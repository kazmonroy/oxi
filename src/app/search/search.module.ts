import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { IconsModule } from '../icons/icons.module';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { SearchTableComponent } from './components/search-table/search-table.component';
import { SearchService } from './services/search.service';
import { SearchDataSource } from './services/search.dataSource';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchFiltersComponent } from '../header/components/search-filters/search-filters.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';

@NgModule({
  declarations: [SearchResultsComponent, SearchTableComponent, SearchBoxComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    HttpClientModule,
    IconsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    ReactiveFormsModule,
  ],
  providers: [SearchService, SearchDataSource],
})
export class SearchModule {}
