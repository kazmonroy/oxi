export interface Results {
  id: number;
  name: string;
  username: string;
  email?: string;
}
