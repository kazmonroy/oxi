import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons/icons.module';

@Component({
  selector: 'app-control-bar',
  standalone: true,
  imports: [CommonModule, IconsModule],
  templateUrl: './control-bar.component.html',
  styleUrls: ['./control-bar.component.scss'],
})
export class ControlBarComponent {}
